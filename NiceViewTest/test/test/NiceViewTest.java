/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import java.util.Date;
import java.util.GregorianCalendar;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import org.junit.Test;
import static org.junit.Assert.*;
import ws.niceviewwsdl.BookHotelFault;
import ws.niceviewwsdl.CancelHotelFault;
import ws.niceviewwsdl.HotelListType;

/**
 *
 * @author maria
 */
public class NiceViewTest {

    private int bookingNumber = 4;

    @Test
    public void getHotelTest() throws DatatypeConfigurationException {
        ws.niceviewwsdl.GetHotelRequest request = new ws.niceviewwsdl.GetHotelRequest();

        GregorianCalendar c = new GregorianCalendar();
        c.setTime(new Date());
        XMLGregorianCalendar arrival = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
        XMLGregorianCalendar departure = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);

        arrival.setDay(4);
        arrival.setMonth(5);
        arrival.setYear(2005);

        departure.setDay(7);
        departure.setMonth(5);
        departure.setYear(2005);

        request.setCity("Madrid");
        request.setArrivalDate(arrival);
        request.setDepartureDate(departure);

        HotelListType hList = getHotel(request);

        assertEquals(3, hList.getHotel().size());

        for (ws.niceviewwsdl.HotelType h : hList.getHotel()) {
            assertEquals("Madrid", h.getAddress().getCity());
        }

        bookingNumber = hList.getHotel().get(0).getBookingNumber();
    }

    @Test
    public void bookHotelTest() throws BookHotelFault {
        ws.niceviewwsdl.BookHotelRequest request = new ws.niceviewwsdl.BookHotelRequest();

        request.setBookingNumber(bookingNumber);
        request.setCreditCardInfo(new ws.niceviewwsdl.CreditCardInfoType());
        request.getCreditCardInfo().setName("Tick Joachim");
        request.getCreditCardInfo().setNumber("50408824");

        ws.niceviewwsdl.CreditCardInfoType.ExpirationDate date = new ws.niceviewwsdl.CreditCardInfoType.ExpirationDate();
        date.setMonth(2);
        date.setYear(11);

        request.getCreditCardInfo().setExpirationDate(date);

        boolean ret = bookHotel(request);

        assertTrue(ret);

    }

    @Test(expected = BookHotelFault.class)
    public void bookHotelFailTest() throws BookHotelFault {
        ws.niceviewwsdl.BookHotelRequest request = new ws.niceviewwsdl.BookHotelRequest();

        request.setBookingNumber(bookingNumber);
        request.setCreditCardInfo(new ws.niceviewwsdl.CreditCardInfoType());
        request.getCreditCardInfo().setName("Klinkby Poul");
        request.getCreditCardInfo().setNumber("50408818");

        ws.niceviewwsdl.CreditCardInfoType.ExpirationDate date = new ws.niceviewwsdl.CreditCardInfoType.ExpirationDate();
        date.setMonth(3);
        date.setYear(10);

        request.getCreditCardInfo().setExpirationDate(date);

        boolean ret = bookHotel(request);

    }
    
    @Test
    public void cancelHotel() throws CancelHotelFault{
        ws.niceviewwsdl.CancelHotelRequest request = new ws.niceviewwsdl.CancelHotelRequest();
        request.setBookingNumber(bookingNumber);
        
        boolean ret = cancelHotel(request);
        assertTrue(ret);
    }

    private static boolean bookHotel(ws.niceviewwsdl.BookHotelRequest bookHotelRequest) throws BookHotelFault {
        ws.niceviewwsdl.NiceViewWSDLService service = new ws.niceviewwsdl.NiceViewWSDLService();
        ws.niceviewwsdl.NiceViewPortType port = service.getNiceViewPortTypeBindingPort();
        return port.bookHotel(bookHotelRequest);
    }

    private static boolean cancelHotel(ws.niceviewwsdl.CancelHotelRequest cancelHotelRequest) throws CancelHotelFault {
        ws.niceviewwsdl.NiceViewWSDLService service = new ws.niceviewwsdl.NiceViewWSDLService();
        ws.niceviewwsdl.NiceViewPortType port = service.getNiceViewPortTypeBindingPort();
        return port.cancelHotel(cancelHotelRequest);
    }

    private static HotelListType getHotel(ws.niceviewwsdl.GetHotelRequest getHotelRequest) {
        ws.niceviewwsdl.NiceViewWSDLService service = new ws.niceviewwsdl.NiceViewWSDLService();
        ws.niceviewwsdl.NiceViewPortType port = service.getNiceViewPortTypeBindingPort();
        return port.getHotel(getHotelRequest);
    }

}
