
package dk.dtu.ws.lameduck;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for flightType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="flightType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="dateTimeDept" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="dateTimeArr" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="airportDept" type="{http://lameduck.ws.dtu.dk}airportType"/>
 *         &lt;element name="airportArr" type="{http://lameduck.ws.dtu.dk}airportType"/>
 *       &lt;/sequence>
 *       &lt;attribute name="carrierName" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "flightType", propOrder = {
    "dateTimeDept",
    "dateTimeArr",
    "airportDept",
    "airportArr"
})
public class FlightType {

    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dateTimeDept;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dateTimeArr;
    @XmlElement(required = true)
    protected AirportType airportDept;
    @XmlElement(required = true)
    protected AirportType airportArr;
    @XmlAttribute(name = "carrierName")
    protected String carrierName;

    /**
     * Gets the value of the dateTimeDept property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateTimeDept() {
        return dateTimeDept;
    }

    /**
     * Sets the value of the dateTimeDept property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateTimeDept(XMLGregorianCalendar value) {
        this.dateTimeDept = value;
    }

    /**
     * Gets the value of the dateTimeArr property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateTimeArr() {
        return dateTimeArr;
    }

    /**
     * Sets the value of the dateTimeArr property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateTimeArr(XMLGregorianCalendar value) {
        this.dateTimeArr = value;
    }

    /**
     * Gets the value of the airportDept property.
     * 
     * @return
     *     possible object is
     *     {@link AirportType }
     *     
     */
    public AirportType getAirportDept() {
        return airportDept;
    }

    /**
     * Sets the value of the airportDept property.
     * 
     * @param value
     *     allowed object is
     *     {@link AirportType }
     *     
     */
    public void setAirportDept(AirportType value) {
        this.airportDept = value;
    }

    /**
     * Gets the value of the airportArr property.
     * 
     * @return
     *     possible object is
     *     {@link AirportType }
     *     
     */
    public AirportType getAirportArr() {
        return airportArr;
    }

    /**
     * Sets the value of the airportArr property.
     * 
     * @param value
     *     allowed object is
     *     {@link AirportType }
     *     
     */
    public void setAirportArr(AirportType value) {
        this.airportArr = value;
    }

    /**
     * Gets the value of the carrierName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCarrierName() {
        return carrierName;
    }

    /**
     * Sets the value of the carrierName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCarrierName(String value) {
        this.carrierName = value;
    }

}
