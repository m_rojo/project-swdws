
package dk.dtu.ws.lameduck;

import java.math.BigInteger;
import javax.xml.ws.WebFault;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.10-b140803.1500
 * Generated source version: 2.2
 * 
 */
@WebFault(name = "failedBookingNrElement", targetNamespace = "http://lameduck.ws.dtu.dk")
public class BookFlightFault
    extends Exception
{

    /**
     * Java type that goes as soapenv:Fault detail element.
     * 
     */
    private BigInteger faultInfo;

    /**
     * 
     * @param faultInfo
     * @param message
     */
    public BookFlightFault(String message, BigInteger faultInfo) {
        super(message);
        this.faultInfo = faultInfo;
    }

    /**
     * 
     * @param faultInfo
     * @param cause
     * @param message
     */
    public BookFlightFault(String message, BigInteger faultInfo, Throwable cause) {
        super(message, cause);
        this.faultInfo = faultInfo;
    }

    /**
     * 
     * @return
     *     returns fault bean: java.math.BigInteger
     */
    public BigInteger getFaultInfo() {
        return faultInfo;
    }

}
