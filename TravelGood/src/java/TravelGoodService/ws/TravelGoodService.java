/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TravelGoodService.ws;

import dk.dtu.ws.travelgood.wsdl.travelgood.java.travelgoodwsdl.BookingFault;
import dk.dtu.ws.travelgood.wsdl.travelgood.java.travelgoodwsdl.CancellItineraryFault;
import javax.jws.WebService;
import dk.dtu.ws.lameduck.*;
import java.sql.*;
import java.util.Random;


/**
 *
 * @author Alvaro
 */
@WebService(serviceName = "TravelGoodWSDLService", portName = "TravelGoodPortTypeBindingPort", endpointInterface = "dk.dtu.ws.travelgood.wsdl.travelgood.java.travelgoodwsdl.TravelGoodPortType", targetNamespace = "http://travelgood.ws.dtu.dk/wsdl/TravelGood/java/TravelGoodWSDL", wsdlLocation = "WEB-INF/wsdl/TravelGoodService/TravelGoodWSDL.wsdl")
public class TravelGoodService {
   
   
    
    public int booking(java.lang.String user, int itineraryNumber, dk.dtu.imm.fastmoney.types.CreditCardInfoType creditCardInfo) throws BookingFault {
        //TODO implement this method
        
        
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public travelgood.ws.dtu.dk.FlightInfoListType getFlights(travelgood.ws.dtu.dk.AirportType departure, travelgood.ws.dtu.dk.AirportType destination, javax.xml.datatype.XMLGregorianCalendar date) {
        //TODO implement this method
       //BAD IMPLEMENTATION 
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public travelgood.ws.dtu.dk.HotelListType getHotels(javax.xml.datatype.XMLGregorianCalendar arrival, javax.xml.datatype.XMLGregorianCalendar departure, java.lang.String city) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public boolean addFlight(int itineraryNumber, int numberFlight) throws ClassNotFoundException, SQLException {
        //TODO implement this method
        //pass the whole information in one obj
        try{
            Connection con1 = GlobalItinerary.ConnetcionDBLite();
            Statement stat = con1.createStatement();
            stat.executeUpdate("INSERT INTO Flights (ItineraryID, FlightNumber) VALUES ('"+itineraryNumber+"','"+numberFlight+"')");
            stat.close();
            con1.close();
            return true;
        }catch(Exception e){
            return false;
        }
        

        //throw new UnsupportedOperationException("Not implemented yet.");
    }

    public boolean addHotel(int itineraryNumber, int numberHotel) {
        //TODO implement this method
        //pass the whole information in one obj
        try{
            Connection con1 = GlobalItinerary.ConnetcionDBLite();
            Statement stat = con1.createStatement();
            stat.executeUpdate("INSERT INTO Hotels (ItineraryID, HotelNumber) VALUES ('"+itineraryNumber+"','"+numberHotel+"')");
            stat.close();
            con1.close();
            return true;
        }catch(Exception e){
            return false;
        }
        
        //throw new UnsupportedOperationException("Not implemented yet.");
    }

    public boolean removeFlight(int itineraryNumber, int numberFlight) {
        //TODO implement this method
        try{
            Connection con1 = GlobalItinerary.ConnetcionDBLite();
            Statement stat = con1.createStatement();
            
            stat.executeUpdate("DELETE FROM Flights WHERE ItineraryID = "+itineraryNumber+" AND FlightNumber = "+numberFlight+" ");
            stat.close();
            con1.close();
            return true;
        }catch(Exception e){
            return false;
        }
        
        //throw new UnsupportedOperationException("Not implemented yet.");
    }

    public boolean removeHotel(int itineraryNumber, int numberHotel) {
        //TODO implement this method
        try{
            Connection con1 = GlobalItinerary.ConnetcionDBLite();
            Statement stat = con1.createStatement();
            
            stat.executeUpdate("DELETE FROM Hotels WHERE ItineraryID = "+itineraryNumber+" AND Hotelnumber = "+numberHotel+"");
            stat.close();
            con1.close();
            return true;
        }catch(Exception e){
            return false;
        }
        //throw new UnsupportedOperationException("Not implemented yet.");
    }

    public int createItinerary(java.lang.String user) throws ClassNotFoundException, SQLException {
        //TODO implement this method
        //this method returns the itinerary number to the client
        Random rand;
        int min = 1000;
        int max = 500000000;
        int itineraryID = rand.nextInt((max - min) + 1) + min;
        Connection con1 = GlobalItinerary.ConnetcionDBLite();
        Statement stat = con1.createStatement();
        stat.executeUpdate("INSERT INTO Itinerary (ItineraryID, User) VALUES ('"+itineraryID+"','"+user+"')");
        stat.close();
        con1.close();
        
        
        return itineraryID;
        
        //throw new UnsupportedOperationException("Not implemented yet.");
    }

    public boolean cancellItinerary(int itineraryNumber, java.lang.String user, travelgood.ws.dtu.dk.CreditCardInfoType returnMoney) throws CancellItineraryFault {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public travelgood.ws.dtu.dk.Itinerary checkItinerary(int itineraryNumer, java.lang.String user) {
        //TODO implement this method
        try{
            Connection con1 = GlobalItinerary.ConnetcionDBLite();
            Statement stat = con1.createStatement();
            Statement stat2 = con1.createStatement();
            ResultSet rs = stat.executeQuery("SELECT * FROM Flights WHERE ItineraryNumber="+"'"+itineraryNumer+"'");
            ResultSet rs2 = stat2.executeQuery("SELECT * FROM Hotels WHERE ItineraryNumber="+"'"+itineraryNumer+"'");
            //create the object structure to send it back
            /*itinerayObj.User = user
            while(rs.next()){
                int ix = 0;
                itineraryObj.flights[ix].flightNumber = rs.getInt(1);
                itineraryObj.flights[ix].departureDate = rs.getDate(2);
                itineraryObj.flights[ix].to = rs.getString(3);
                itineraryObj.flights[ix].from = rs.getString(4);
                ix++;        
            }
            while(rs2.next()){
                int ix = 0;         
                itineraryObj.hotesl[ix].flightNumber = rs.getInt(1);
                itineraryObj.hotesl[ix].departureDate = rs.getDate(2);
                itineraryObj.hotesl[ix].to = rs.getString(3);
                itineraryObj.hotesl[ix].from = rs.getString(4);
                ix++;        
            }*/
            
            stat.close();
            con1.close();
            
            
            //return itineraryObj
        }catch(Exception e){
            
        }
        //throw new UnsupportedOperationException("Not implemented yet.");
    }
    
}
