/*
Service implementation for the flight agency LameDuck.
 */
package dk.dtu.ws.lameduck;

import dk.dtu.imm.fastmoney.BankService;
import dk.dtu.imm.fastmoney.CreditCardFaultMessage;
import dk.dtu.imm.fastmoney.types.AccountType;
import dk.dtu.imm.fastmoney.types.CreditCardInfoType;
import java.util.ArrayList;
import java.util.HashMap;
import javax.jws.WebService;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.WebServiceRef;

/**
 *
 * @author Enrico Cimitan - s151414
 */
@WebService(serviceName = "LameDuckService", portName = "LameDuckPort", endpointInterface = "dk.dtu.ws.lameduck.LameDuckPortType", targetNamespace = "http://lameduck.ws.dtu.dk", wsdlLocation = "WEB-INF/wsdl/LameDuckServiceFromWSDL/LameDuckWSDL.wsdl")
public class LameDuckServiceFromWSDL {
//@WebServiceRef(wsdlLocation = "WEB-INF/wsdl/fastmoney.imm.dtu.dk_8080/BankService.wsdl")
    private BankService service = new BankService();
    
    private static final HashMap<Integer, FlightInfoType> generatedFlights = new HashMap<Integer, FlightInfoType>();
    private static int bookingCounter = 1000;
    private static final AccountType ourAccount = initializeAccount();

    private static AccountType initializeAccount () {
        AccountType ret = new AccountType();
        ret.setName("LameDuck");
        ret.setNumber("50208812");
        return ret;
    }
    
    public dk.dtu.ws.lameduck.FlightInfoListType getFlights(dk.dtu.ws.lameduck.GetFlightsInput input) {
        AirportType airportStart = input.getAirportStart();
        AirportType airportDest = input.getAirportDest();
        XMLGregorianCalendar dateStart = input.getDateStart();
        String carriername = null;
        boolean errordetected = false;
        switch (airportStart.getCity()) {
            case "Valencia":
                carriername = "Iberia"; break;
            case "Venezia":
                carriername = "Alitalia"; break;
            case "Copenhagen":
                carriername = "DAT"; break;
            case "Dusseldorf":
                carriername = "Lufthansa"; break;
            default:
                errordetected = true; break;
        }
        
        switch (airportDest.getCity()) {
            case "Valencia":
            case "Venezia":
            case "Copenhagen":
            case "Dusseldorf": break;
            default:
                errordetected = true; break;
        }
        
        FlightInfoListType ret = new FlightInfoListType();
        ret.flightInfo = new ArrayList<FlightInfoType>();
        FlightInfoType possibleflight;
        FlightType flight;
        DatatypeFactory dtf = null;
        try {
            dtf = DatatypeFactory.newInstance();
        } catch (DatatypeConfigurationException ex) {
            errordetected = true;
        }
        
        if (errordetected || airportDest.getCity().equals(airportStart.getCity())) {return ret;}
        
        for (int i = 0; i <= 2; i++) { /*adds three hardcoded elements*/
            flight = new FlightType();
            flight.setAirportArr(airportDest);
            flight.setAirportDept(airportStart);
            flight.setCarrierName(carriername);
            flight.setDateTimeDept(dtf.newXMLGregorianCalendar(
                    dateStart.getYear(),
                    dateStart.getMonth(),
                    dateStart.getDay(),
                    12 + i, 30, 00, 00, 00));
            flight.setDateTimeArr(dtf.newXMLGregorianCalendar(
                    dateStart.getYear(),
                    dateStart.getMonth(),
                    dateStart.getDay(),
                    12 + 3 + i, 00, 00, 00, 00));
            
            possibleflight = new FlightInfoType();
            possibleflight.setAirlineResName("LameDuckCheapReservations");
            possibleflight.setPrice(29 + (int)(970 * Math.random())   );
            possibleflight.setBookingNr(bookingCounter);
            possibleflight.setFlightChosen(flight);
            
            ret.flightInfo.add(possibleflight);
            generatedFlights.put(bookingCounter, possibleflight);
            bookingCounter++;
        }
        
        return ret;
    }

    public boolean bookFlight(dk.dtu.ws.lameduck.BookFlightInput input) throws BookFlightFault {
        int bookingNr = input.getBookingNr();
        CreditCardInfoType creditCardInfo = input.getCreditCardInfo();
        FlightInfoType fi = generatedFlights.get(bookingNr);
        
        if (fi == null) {
            throw new BookFlightFault("Booking Number Not Found!", bookingNr);
        }
        
        boolean succesful = true;
        
        try {
            succesful = chargeCreditCard(6, creditCardInfo, fi.getPrice(), ourAccount);
        } catch (CreditCardFaultMessage ex) {
            throw new BookFlightFault("Error while charging the credit card!", bookingNr, ex);
        }
        
        if (!succesful) throw new BookFlightFault("Generic Error from bank server! Try again later!", bookingNr);
        
        return true;
    }

    public boolean cancelFlight(dk.dtu.ws.lameduck.CancelFlightInput input) throws CancelFlightFault {
        CreditCardInfoType creditCardInfo = input.getCreditCardInfo();
        int price = input.getPrice();
        int bookingNr = input.getBookingNr();
        boolean succesful = true;
        try {
            succesful = refundCreditCard(6, creditCardInfo, price/2 , ourAccount);
        } catch (CreditCardFaultMessage ex) {
            throw new CancelFlightFault("Impossible to complete a refund from the bank!", bookingNr, ex);
        }
        
        if (!succesful) throw new CancelFlightFault("Generic Error from bank server! Try again later!", bookingNr);
        
        return true;
    }
    
    /*
    Stub client methods from bank service:
    */

    private boolean chargeCreditCard(int group, dk.dtu.imm.fastmoney.types.CreditCardInfoType creditCardInfo, int amount, dk.dtu.imm.fastmoney.types.AccountType account) throws CreditCardFaultMessage {
        // Note that the injected javax.xml.ws.Service reference as well as port objects are not thread safe.
        // If the calling of port operations may lead to race condition some synchronization is required.
        dk.dtu.imm.fastmoney.BankPortType port = service.getBankPort();
        return port.chargeCreditCard(group, creditCardInfo, amount, account);
    }

    private boolean refundCreditCard(int group, dk.dtu.imm.fastmoney.types.CreditCardInfoType creditCardInfo, int amount, dk.dtu.imm.fastmoney.types.AccountType account) throws CreditCardFaultMessage {
        // Note that the injected javax.xml.ws.Service reference as well as port objects are not thread safe.
        // If the calling of port operations may lead to race condition some synchronization is required.
        dk.dtu.imm.fastmoney.BankPortType port = service.getBankPort();
        return port.refundCreditCard(group, creditCardInfo, amount, account);
    }
    
    
}
