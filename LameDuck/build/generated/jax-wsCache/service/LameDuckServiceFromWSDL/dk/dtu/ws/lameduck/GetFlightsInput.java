
package dk.dtu.ws.lameduck;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for getFlightsInput complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getFlightsInput">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="airportStart" type="{http://lameduck.ws.dtu.dk}airportType"/>
 *         &lt;element name="airportDest" type="{http://lameduck.ws.dtu.dk}airportType"/>
 *         &lt;element name="dateStart" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getFlightsInput", propOrder = {
    "airportStart",
    "airportDest",
    "dateStart"
})
public class GetFlightsInput {

    @XmlElement(required = true)
    protected AirportType airportStart;
    @XmlElement(required = true)
    protected AirportType airportDest;
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dateStart;

    /**
     * Gets the value of the airportStart property.
     * 
     * @return
     *     possible object is
     *     {@link AirportType }
     *     
     */
    public AirportType getAirportStart() {
        return airportStart;
    }

    /**
     * Sets the value of the airportStart property.
     * 
     * @param value
     *     allowed object is
     *     {@link AirportType }
     *     
     */
    public void setAirportStart(AirportType value) {
        this.airportStart = value;
    }

    /**
     * Gets the value of the airportDest property.
     * 
     * @return
     *     possible object is
     *     {@link AirportType }
     *     
     */
    public AirportType getAirportDest() {
        return airportDest;
    }

    /**
     * Sets the value of the airportDest property.
     * 
     * @param value
     *     allowed object is
     *     {@link AirportType }
     *     
     */
    public void setAirportDest(AirportType value) {
        this.airportDest = value;
    }

    /**
     * Gets the value of the dateStart property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateStart() {
        return dateStart;
    }

    /**
     * Sets the value of the dateStart property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateStart(XMLGregorianCalendar value) {
        this.dateStart = value;
    }

}
