
package dk.dtu.ws.lameduck;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the dk.dtu.ws.lameduck package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _FailedBookingNrElement_QNAME = new QName("http://lameduck.ws.dtu.dk", "failedBookingNrElement");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: dk.dtu.ws.lameduck
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CancelFlightInput }
     * 
     */
    public CancelFlightInput createCancelFlightInput() {
        return new CancelFlightInput();
    }

    /**
     * Create an instance of {@link FlightType }
     * 
     */
    public FlightType createFlightType() {
        return new FlightType();
    }

    /**
     * Create an instance of {@link FlightInfoType }
     * 
     */
    public FlightInfoType createFlightInfoType() {
        return new FlightInfoType();
    }

    /**
     * Create an instance of {@link BookFlightInput }
     * 
     */
    public BookFlightInput createBookFlightInput() {
        return new BookFlightInput();
    }

    /**
     * Create an instance of {@link FlightInfoListType }
     * 
     */
    public FlightInfoListType createFlightInfoListType() {
        return new FlightInfoListType();
    }

    /**
     * Create an instance of {@link GetFlightsInput }
     * 
     */
    public GetFlightsInput createGetFlightsInput() {
        return new GetFlightsInput();
    }

    /**
     * Create an instance of {@link AirportType }
     * 
     */
    public AirportType createAirportType() {
        return new AirportType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://lameduck.ws.dtu.dk", name = "failedBookingNrElement")
    public JAXBElement<Integer> createFailedBookingNrElement(Integer value) {
        return new JAXBElement<Integer>(_FailedBookingNrElement_QNAME, Integer.class, null, value);
    }

}
