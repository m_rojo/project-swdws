
package ws.niceviewwsdl;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ws.niceviewwsdl package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _BookHotelFault_QNAME = new QName("http://NiceViewWSDL.ws", "bookHotelFault");
    private final static QName _BookHotelResponse_QNAME = new QName("http://NiceViewWSDL.ws", "bookHotelResponse");
    private final static QName _CancelHotelResponse_QNAME = new QName("http://NiceViewWSDL.ws", "cancelHotelResponse");
    private final static QName _CancelHotelFault_QNAME = new QName("http://NiceViewWSDL.ws", "cancelHotelFault");
    private final static QName _GetHotelResponse_QNAME = new QName("http://NiceViewWSDL.ws", "getHotelResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ws.niceviewwsdl
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CreditCardInfoType }
     * 
     */
    public CreditCardInfoType createCreditCardInfoType() {
        return new CreditCardInfoType();
    }

    /**
     * Create an instance of {@link BookHotelRequest }
     * 
     */
    public BookHotelRequest createBookHotelRequest() {
        return new BookHotelRequest();
    }

    /**
     * Create an instance of {@link CancelHotelFaultType }
     * 
     */
    public CancelHotelFaultType createCancelHotelFaultType() {
        return new CancelHotelFaultType();
    }

    /**
     * Create an instance of {@link HotelListType }
     * 
     */
    public HotelListType createHotelListType() {
        return new HotelListType();
    }

    /**
     * Create an instance of {@link GetHotelRequest }
     * 
     */
    public GetHotelRequest createGetHotelRequest() {
        return new GetHotelRequest();
    }

    /**
     * Create an instance of {@link CancelHotelRequest }
     * 
     */
    public CancelHotelRequest createCancelHotelRequest() {
        return new CancelHotelRequest();
    }

    /**
     * Create an instance of {@link BookHotelFaultType }
     * 
     */
    public BookHotelFaultType createBookHotelFaultType() {
        return new BookHotelFaultType();
    }

    /**
     * Create an instance of {@link AddressType }
     * 
     */
    public AddressType createAddressType() {
        return new AddressType();
    }

    /**
     * Create an instance of {@link HotelType }
     * 
     */
    public HotelType createHotelType() {
        return new HotelType();
    }

    /**
     * Create an instance of {@link CreditCardInfoType.ExpirationDate }
     * 
     */
    public CreditCardInfoType.ExpirationDate createCreditCardInfoTypeExpirationDate() {
        return new CreditCardInfoType.ExpirationDate();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BookHotelFaultType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://NiceViewWSDL.ws", name = "bookHotelFault")
    public JAXBElement<BookHotelFaultType> createBookHotelFault(BookHotelFaultType value) {
        return new JAXBElement<BookHotelFaultType>(_BookHotelFault_QNAME, BookHotelFaultType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://NiceViewWSDL.ws", name = "bookHotelResponse")
    public JAXBElement<Boolean> createBookHotelResponse(Boolean value) {
        return new JAXBElement<Boolean>(_BookHotelResponse_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://NiceViewWSDL.ws", name = "cancelHotelResponse")
    public JAXBElement<Boolean> createCancelHotelResponse(Boolean value) {
        return new JAXBElement<Boolean>(_CancelHotelResponse_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CancelHotelFaultType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://NiceViewWSDL.ws", name = "cancelHotelFault")
    public JAXBElement<CancelHotelFaultType> createCancelHotelFault(CancelHotelFaultType value) {
        return new JAXBElement<CancelHotelFaultType>(_CancelHotelFault_QNAME, CancelHotelFaultType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HotelListType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://NiceViewWSDL.ws", name = "getHotelResponse")
    public JAXBElement<HotelListType> createGetHotelResponse(HotelListType value) {
        return new JAXBElement<HotelListType>(_GetHotelResponse_QNAME, HotelListType.class, null, value);
    }

}
