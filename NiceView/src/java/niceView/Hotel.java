package niceView;

/**
 *
 * @author Maria Rojo
 */
public class Hotel {
    private String name;
    private String street;
    private String postCode;
    private String city;
    private int bookingNumber;
    private double price;
    private boolean creditCardGuarantee;
    public static final String serviceName = "NiceView";

    public Hotel(String name, String street, String postCode, String city, int bookingNumber, double price, boolean creditCardGuarantee) {
        this.name = name;
        this.street = street;
        this.postCode = postCode;
        this.city = city;
        this.bookingNumber = bookingNumber;
        this.price = price;
        this.creditCardGuarantee = creditCardGuarantee;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getBookingNumber() {
        return bookingNumber;
    }

    public void setBookingNumber(int bookingNumber) {
        this.bookingNumber = bookingNumber;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public boolean isCreditCardGuarantee() {
        return creditCardGuarantee;
    }

    public void setCreditCardGuarantee(boolean creditCardGuarantee) {
        this.creditCardGuarantee = creditCardGuarantee;
    }
}
