package niceView;

import dk.dtu.imm.fastmoney.BankService;
import dk.dtu.imm.fastmoney.CreditCardFaultMessage;
import java.util.ArrayList;
import javax.annotation.PostConstruct;
import javax.jws.WebService;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.WebServiceRef;

/**
 *
 * @author maria
 */
@WebService(serviceName = "NiceViewWSDLService", portName = "NiceViewPortTypeBindingPort", endpointInterface = "ws.niceviewwsdl.NiceViewPortType", targetNamespace = "http://NiceViewWSDL.ws", wsdlLocation = "WEB-INF/wsdl/NiceView/NiceViewWSDL.wsdl")
public class NiceView {

    @WebServiceRef(wsdlLocation = "WEB-INF/wsdl/fastmoney.imm.dtu.dk_8080/BankService.wsdl")
    private BankService service;

    public ArrayList<Hotel> hotelList;
    public ArrayList<Hotel> bookedHotelList;

    @PostConstruct
    public void init() {
        hotelList = new ArrayList<>();
        bookedHotelList = new ArrayList<>();

        hotelList.add(new Hotel("Scandic Hotel Copenhagen", "Vester Søgade 6", "1601", "Copenhagen", 1, 1133, true));
        hotelList.add(new Hotel("Danhostel Copenhagen City", "H. C. Andersens Blvd. 50", "1553", "Copenhagen", 2, 500, false));
        hotelList.add(new Hotel("Hotel Kong Arthur", "Nørre Søgade 11", "1370", "Copenhagen", 3, 1057, false));
        hotelList.add(new Hotel("The Westin Palace", "Plaza de las Cortes, 7", "28014", "Madrid", 4, 2421, true));
        hotelList.add(new Hotel("Hotel Ritz", "Plaza de la Lealtad, 5", "28014", "Madrid", 5, 2611, true));
        hotelList.add(new Hotel("Hotel Regina", "Calle Alcala 19", "28014", "Madrid", 6, 410, false));
    }

    public ws.niceviewwsdl.HotelListType getHotel(ws.niceviewwsdl.GetHotelRequest getHotelRequest) {
        ws.niceviewwsdl.HotelListType resultList = new ws.niceviewwsdl.HotelListType();

        XMLGregorianCalendar arrival = getHotelRequest.getArrivalDate();
        XMLGregorianCalendar departure = getHotelRequest.getDepartureDate();
        int nDays = departure.getDay() - arrival.getDay();

        for (Hotel h : hotelList) {
            if (h.getCity().contentEquals(getHotelRequest.getCity())) {
                ws.niceviewwsdl.HotelType newH = new ws.niceviewwsdl.HotelType();
                newH.setName(h.getName());
                newH.setAddress(new ws.niceviewwsdl.AddressType());
                newH.getAddress().setStreet(h.getStreet());
                newH.getAddress().setPostCode(h.getPostCode());
                newH.getAddress().setCity(h.getCity());
                newH.setBookingNumber(h.getBookingNumber());
                newH.setPrice(nDays * h.getPrice());
                newH.setCreditCardGuarantee(h.isCreditCardGuarantee());
                newH.setServiceName(Hotel.serviceName);
                resultList.getHotel().add(newH);
            }
        }
        return resultList;
    }

    public boolean bookHotel(ws.niceviewwsdl.BookHotelRequest bookHotelRequest) throws ws.niceviewwsdl.BookHotelFault {
        int bookingNumber = bookHotelRequest.getBookingNumber();

        for (Hotel h : hotelList) {
            if (bookingNumber == h.getBookingNumber()) {
                if (h.isCreditCardGuarantee()) {
                    dk.dtu.imm.fastmoney.types.CreditCardInfoType info = new dk.dtu.imm.fastmoney.types.CreditCardInfoType();
                    info.setName(bookHotelRequest.getCreditCardInfo().getName());
                    info.setNumber(bookHotelRequest.getCreditCardInfo().getNumber());
                    dk.dtu.imm.fastmoney.types.CreditCardInfoType.ExpirationDate date = new dk.dtu.imm.fastmoney.types.CreditCardInfoType.ExpirationDate();
                    date.setMonth(bookHotelRequest.getCreditCardInfo().getExpirationDate().getMonth());
                    date.setYear(bookHotelRequest.getCreditCardInfo().getExpirationDate().getYear());
                    info.setExpirationDate(date);
                    try {
                        if (!validateCreditCard(6, info, (int) h.getPrice())) {
                            throw new ws.niceviewwsdl.BookHotelFault("Invalid credit card", new ws.niceviewwsdl.BookHotelFaultType());
                        } else {
                            bookedHotelList.add(h);
                            return true;
                        }
                    } catch (CreditCardFaultMessage ex) {
                        throw new ws.niceviewwsdl.BookHotelFault(ex.toString(), new ws.niceviewwsdl.BookHotelFaultType());
                    }
                }
            }
        }

        throw new ws.niceviewwsdl.BookHotelFault("Invalid booking number", null);
    }

    public boolean cancelHotel(ws.niceviewwsdl.CancelHotelRequest cancelHotelRequest) throws ws.niceviewwsdl.CancelHotelFault {
        int bookingNumber = cancelHotelRequest.getBookingNumber();
        for (Hotel h : hotelList) {
            if (bookingNumber == h.getBookingNumber()) {
                bookedHotelList.remove(h);
                return true;
            }
        }
        throw new ws.niceviewwsdl.CancelHotelFault("invalid booking number", null);
    }

    private boolean chargeCreditCard(int group, dk.dtu.imm.fastmoney.types.CreditCardInfoType creditCardInfo, int amount, dk.dtu.imm.fastmoney.types.AccountType account) throws CreditCardFaultMessage {
        // Note that the injected javax.xml.ws.Service reference as well as port objects are not thread safe.
        // If the calling of port operations may lead to race condition some synchronization is required.
        dk.dtu.imm.fastmoney.BankPortType port = service.getBankPort();
        return port.chargeCreditCard(group, creditCardInfo, amount, account);
    }

    private boolean refundCreditCard(int group, dk.dtu.imm.fastmoney.types.CreditCardInfoType creditCardInfo, int amount, dk.dtu.imm.fastmoney.types.AccountType account) throws CreditCardFaultMessage {
        // Note that the injected javax.xml.ws.Service reference as well as port objects are not thread safe.
        // If the calling of port operations may lead to race condition some synchronization is required.
        dk.dtu.imm.fastmoney.BankPortType port = service.getBankPort();
        return port.refundCreditCard(group, creditCardInfo, amount, account);
    }

    private boolean validateCreditCard(int group, dk.dtu.imm.fastmoney.types.CreditCardInfoType creditCardInfo, int amount) throws CreditCardFaultMessage {
        // Note that the injected javax.xml.ws.Service reference as well as port objects are not thread safe.
        // If the calling of port operations may lead to race condition some synchronization is required.
        dk.dtu.imm.fastmoney.BankPortType port = service.getBankPort();
        return port.validateCreditCard(group, creditCardInfo, amount);
    }

}
