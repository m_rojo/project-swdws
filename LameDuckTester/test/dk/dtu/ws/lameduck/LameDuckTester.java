/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.dtu.ws.lameduck;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Enrico
 */
public class LameDuckTester {
    
    public LameDuckTester() {
    }
    
    
    @Test
    public void testMethod() throws DatatypeConfigurationException, BookFlightFault, CancelFlightFault {
        AirportType source = new AirportType();
        source.setCity("Venezia"); source.setName("VCE");
        AirportType dest = new AirportType();
        dest.setCity("Valencia"); dest.setName("VLC");
        
        DatatypeFactory dtf = DatatypeFactory.newInstance();
        
        GetFlightsInput flightsInput = new GetFlightsInput();
        flightsInput.setAirportStart(source);
        flightsInput.setAirportDest(dest);
        flightsInput.setDateStart(dtf.newXMLGregorianCalendarDate(2015, 12, 24, 00));
        
        FlightInfoListType flightlist = getFlights(flightsInput);
        CreditCardInfoType crcard = new CreditCardInfoType();
        CreditCardInfoType.ExpirationDate ed = new CreditCardInfoType.ExpirationDate();
        ed.setMonth(3); ed.setYear(10);
        crcard.setExpirationDate(ed);
        crcard.setName("Klinkby Poul");
        crcard.setNumber("50408817");
                
        assertEquals(flightlist.getFlightInfo().get(1).getAirlineResName(), "LameDuckCheapReservations");
        assertTrue(30 <= flightlist.getFlightInfo().get(1).getPrice());
        assertEquals(flightlist.getFlightInfo().get(1).getFlightChosen().getCarrierName(), "Alitalia");
        assertTrue(flightlist.getFlightInfo().get(1).getBookingNr() > 1000);
        
        BookFlightInput bookFlightInput = new BookFlightInput();
        bookFlightInput.setBookingNr(flightlist.getFlightInfo().get(1).getBookingNr());
        bookFlightInput.setCreditCardInfo(crcard);
        boolean result = bookFlight(bookFlightInput);
        
        if (!result) assertTrue(false);
        
        CancelFlightInput cancelFlightInput = new CancelFlightInput();
        cancelFlightInput.setBookingNr(flightlist.getFlightInfo().get(1).getBookingNr());
        cancelFlightInput.setCreditCardInfo(crcard);
        cancelFlightInput.setPrice(flightlist.getFlightInfo().get(1).getPrice());
    
        if (!result) assertTrue(false);
    
    }
    
    /*
    Stub client methods for the service:
    */

    private static FlightInfoListType getFlights(dk.dtu.ws.lameduck.GetFlightsInput input) {
        dk.dtu.ws.lameduck.LameDuckService service = new dk.dtu.ws.lameduck.LameDuckService();
        dk.dtu.ws.lameduck.LameDuckPortType port = service.getLameDuckPort();
        return port.getFlights(input);
    }

    private static boolean bookFlight(dk.dtu.ws.lameduck.BookFlightInput input) throws BookFlightFault {
        dk.dtu.ws.lameduck.LameDuckService service = new dk.dtu.ws.lameduck.LameDuckService();
        dk.dtu.ws.lameduck.LameDuckPortType port = service.getLameDuckPort();
        return port.bookFlight(input);
    }

    private static boolean cancelFlight(dk.dtu.ws.lameduck.CancelFlightInput input) throws CancelFlightFault {
        dk.dtu.ws.lameduck.LameDuckService service = new dk.dtu.ws.lameduck.LameDuckService();
        dk.dtu.ws.lameduck.LameDuckPortType port = service.getLameDuckPort();
        return port.cancelFlight(input);
    }
    

    
}